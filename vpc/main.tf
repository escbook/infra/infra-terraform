locals {
  availability_zones = ["${var.aws_region}a", "${var.aws_region}b"]
}

# ------------------------------ VPC ------------------------------
resource "aws_vpc" "ut-vpc" {
  cidr_block = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = {
    Name = "${var.environnement}-vpc"
    Environment = "${var.environnement}"
  }
}

# ------------------------------ Public Subnets ------------------------------
resource "aws_subnet" "ut-public-subnet" {
  vpc_id = aws_vpc.ut-vpc.id
  count = length(var.public_subnets_cidr)
  cidr_block = element(var.public_subnets_cidr, count.index)
  availability_zone = element(local.availability_zones, count.index)
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.environnement}-${element(local.availability_zones, count.index)}-public-subnet"
    Environment = "${var.environnement}"
  }
}

# ------------------------------ Private Subnets ------------------------------
resource "aws_subnet" "ur-private-subnet" {
  vpc_id = aws_vpc.ut-vpc.id
  count = length(var.private_subnets_cidr)
  cidr_block = element(var.private_subnets_cidr, count.index)
  availability_zone = element(local.availability_zones, count.index)
  map_public_ip_on_launch = false

  tags = {
    Name = "${var.environnement}-${element(local.availability_zones, count.index)}-private-subnet"
    Environment = "${var.environnement}"
  }
}

# ------------------------------ Internet Gateway ------------------------------
resource "aws_internet_gateway" "ur-internet-gateway" {
  vpc_id = aws_vpc.ut-vpc.id

  tags = {
    Name = "${var.environnement}-igw"
    Environment = "${var.environnement}"
  }
}

# ------------------------------ Elastic IP ------------------------------
resource "aws_eip" "ut-eip" {
  domain = "vpc"
  depends_on = [aws_internet_gateway.ur-internet-gateway]
}

# ------------------------------ NAT Gateway ------------------------------
resource "aws_nat_gateway" "ut-nat" {
  allocation_id = aws_eip.ut-eip.id
  subnet_id     = element(aws_subnet.ut-public-subnet.*.id, 0)

  tags = {
    Name = "${var.environnement}-nat-gateway"
    Environment = "${var.environnement}"
  }
}

# ------------------------------ Route Table Private ------------------------------
resource "aws_route_table" "ut-private" {
  vpc_id = aws_vpc.ut-vpc.id

  tags = {
    Name = "${var.environnement}-private-route-table"
    Environment = "${var.environnement}"
  }
}

# ------------------------------ Route Table Public ------------------------------
resource "aws_route_table" "ut-public" {
  vpc_id = aws_vpc.ut-vpc.id

  tags = {
    Name = "${var.environnement}-public-route-table"
    Environment = "${var.environnement}"
  }
}

# ------------------------------ Route Public NAT Gateway ------------------------------
resource "aws_route" "ut-public-internet-gateway" {
  route_table_id = aws_route_table.ut-public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.ur-internet-gateway.id
}

# ------------------------------ Route Private NAT Gateway ------------------------------
resource "aws_route" "ut-private-internet-gateway" {
  route_table_id = aws_route_table.ut-private.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_nat_gateway.ut-nat.id
}

# ------------------------------ Route Table associations for Public Subnet ------------------------------
resource "aws_route_table_association" "ut-public" {
  count = length(var.public_subnets_cidr)
  subnet_id = element(aws_subnet.ut-public-subnet.*.id, count.index)
  route_table_id = aws_route_table.ut-public.id
}

# ------------------------------ Route Table associations for Private Subnet ------------------------------
resource "aws_route_table_association" "ut-private" {
  count = length(var.private_subnets_cidr)
  subnet_id = element(aws_subnet.ur-private-subnet.*.id, count.index)
  route_table_id = aws_route_table.ut-private.id
}