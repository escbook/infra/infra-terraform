variable "aws_region" {
  type = string
}

variable "vpc_cidr" {
  type = string
  description = "CIDR bloc pour le VPC"
}

variable "public_subnets_cidr" {
  type = list(any)
  description = "CIDR bloc pour le sous-réseau publique"
}

variable "private_subnets_cidr" {
  type = list(any)
  description = "CIDR bloc pour le sous-réseau privé"
}

variable "environnement" {
  type = string
}