output "ut-private-subnet-1-id" {
  value = aws_subnet.ur-private-subnet.0.id
}

output "ut-private-subnet-2-id" {
  value = aws_subnet.ur-private-subnet.1.id
}

output "ut-public-subnet-1-id" {
  value = aws_subnet.ut-public-subnet.0.id
}

output "ut-public-subnet-2-id" {
  value = aws_subnet.ut-public-subnet.1.id
}

output "vpc_id" {
  value = aws_vpc.ut-vpc.id
}