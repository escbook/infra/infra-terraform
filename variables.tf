variable "aws_region" {
  default = "us-east-1"
}

variable "vpc_cidr" {
  type = string
  default = "10.0.0.0/16"
  description = "CIDR bloc pour le VPC"
}

variable "public_subnets_cidr" {
  type = list(any)
  default = ["10.0.0.0/20", "10.0.128.0/20"]
  description = "CIDR bloc pour le sous-réseau publique"
}

variable "private_subnets_cidr" {
  type = list(any)
  default = ["10.0.16.0/20", "10.0.144.0/20"]
  description = "CIDR bloc pour le sous-réseau privé"
}

variable "cidr_blocks_bastion_host" {
  type = list(any)
  default = ["10.0.4.0/24"]
}

variable "environnement" {
  type = string
  default = "urban-threads"
}
