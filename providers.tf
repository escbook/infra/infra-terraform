provider "aws" {
  region = var.aws_region
}

terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 5.31.0"
    }

    kubernetes = {
      source = "hashicorp/kubernetes"
      version = ">= 2.25.2"
    }
  }
}
